class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :name
      t.datetime :start
      t.datetime :end
      t.integer :max_players
      t.decimal :ticket_cost
      t.integer :renown_award

      t.timestamps
    end
  end
end
