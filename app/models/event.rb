class Event < ApplicationRecord
  validate :valid_end_date
  validate :valid_start_date

  validate :name,   lenght: { maximum: 60, minimum: 8 }
  validate :start,  presence: true
  validate :end,    presence: true
  validate :max_players, numericality: { only_integer: true, greater_than: 0 }

  validate :ticket_cost, presence: true, numericality: {
    greater_than_or_equal_to: 0,
    less_than_or_equal_to: 300
  }

  validate :renown_award, :allow_nil => true, numericality: { only_integer: true }

  def valid_end_date
    errors.add(:end, 'must be a valid end date') if (:end < :start or DateTime.now > :end)
  end

  def valid_start_date
    errors.add(:start, 'must e in the future') if (:start < DateTime.now)  
  end
end
