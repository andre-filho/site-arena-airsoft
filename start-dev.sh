#!/bin/bash

bundle check || bundle update

while ! pg_isready -h arena-database -p 5432 -q -U postgres; do
  >&2 echo "Postgres is unavailable"
  sleep 1
done

>&2 echo "Postgres is up"

if bundle exec rake db:exists; then
  bundle exec rake db:create
  >&2 echo "==========================Created database======================="
  bundle exec rails db:migrate
  >&2 echo "=======================Migrated database======================="
else
  bundle exec rake db:migrate
  >&2 echo "=======================Migrated database======================="  
fi

bundle exec rake db:seed
>&2 echo "=======================Seeded database======================="

bundle exec rails s -p 3000 -b '0.0.0.0'
